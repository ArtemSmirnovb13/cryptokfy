<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('CryptOne', 'CryptOneController');
Route::post('crypt','CryptOneController@crypt')->name("CryptOne.crypt");
Route::resource('CryptTwo', 'CryptTwoController');
Route::post('crypt_two','CryptTwoController@crypt')->name("CryptTwo.crypt");
Route::get('crypt_tree','CryptTreeController@index')->name("CryptTree.crypt");
Route::post('crypt_tree_crypt','CryptTreeController@crypt')->name("CryptTree.crypt.go");
Route::get('crypt_tree_ru','CryptTreeControllerRu@index')->name("CryptTree.crypt.ru");
Route::post('crypt_tree_crypt_rus','CryptTreeControllerRu@crypt')->name("CryptTree.crypt.rus");
Route::get('vernam','CryptTreeController@ver_index')->name("CryptTree.Vernam");
Route::post('vernam_crypt','CryptTreeController@ver_crypt')->name("CryptTree.Vernam.ver_crypt");
Route::get('crypt_five','CryptFiveController@index')->name("CryptFive");
Route::post('crypt_five_crypting','CryptFiveController@crypting')->name("CryptFive.crypting");
Route::get('rsa','RSAController@index')->name("rsa");
Route::post('rsa_crypting','RSAController@RSA')->name("RSA.crypting");
Route::get('aes','AESController@index')->name("aes");
Route::post('aes_crypting','AESController@AES')->name("AES.crypting");
