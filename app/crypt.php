<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crypt extends Model
{
    public static $let_arr_EN = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    public static function GetBytes($get)
    {
        $getbytes = [];
        for ($i = 0; $i < strlen($get); $i++) {
            $getbytes[$i] = ord($get[$i]);
        }
        return $getbytes;
    }
    public static function GetChar($get)
    {

        $getchar = [];
        for ($i = 0; $i < count($get); $i++) {
            $getchar[$i] = chr($get[$i]);
        }

        return $getchar;
    }

    public static function getChars($text)
    {
        $str = str_replace(' ', '', $text);
        $str = mb_strtoupper($str);
        $str = str_split($str);
        return $str;
    }

    public static function Decryption($text,$key){
        $autokey = [];
        $decryptText =[];
        for ($i = 0; $i < count($key); $i++) {
            $autokey[$i] = $key[$i];
        }
        for ($i = 0; $i < count($text) - count($key); $i++) {
            $autokey[$i + count($key)] = 0;
        }

        for ($i = 0; $i < count($text); $i++) {

            $decryptText[$i] = self::getAlphavitCharDecEn(self::getDecryptIndex(self::get_Alphavit_Index_dec($text[$i]),self::get_Alphavit_Index_dec($autokey[$i]))+1);
            if (($i + count($key)) < count($autokey)) {
                $autokey[$i + count($key)] = $decryptText[$i];
            }

        }

        return $decryptText;
    }

    public static function get_index($text)
    {

        $autokey = [];
        $decryptText =[];
        for ($i = 0; $i < count($text); $i++) {
            $decryptText[$i] = self::get_Alphavit_Index_dec($text[$i]);
        }


        return $decryptText;
    }

    public static function getDecryptIndex($encryptIndex, $keyIndex){
        $relust = 0;

        if ($encryptIndex >= $keyIndex){
            $relust = $encryptIndex - $keyIndex;
        }
        else {
            $relust = ( $encryptIndex + count(self::$let_arr_EN)) - $keyIndex;
        }

        if (($relust - 1) == -1) {
            $relust = 26;
        }
        return $relust - 1;
    }

    public static function Encryption($text, $key)
    {

        $autokey = self::getAutoKey($text, $key);

        $encryptText = [];

        for ($i = 0; $i < count($text); $i++) {

            $encryptText[$i] = self::getAlphavitChar(self::getEncryptIndex(self::get_Alphavit_Index($text[$i]), self::get_Alphavit_Index($autokey[$i])));
        }

        return $encryptText;
    }

    public static function getAlphavitChar($index)
    {
        if ($index != 0) {
            return self::$let_arr_EN[$index - 1];
        } else {
            return self::$let_arr_EN[$index];
        }
    }

    public static function getAlphavitCharDecEn($index) {

       // return self::$let_arr_EN[$index];
        if ($index >= 26) {
            return self::$let_arr_EN[$index = 0];
        } else {
            return self::$let_arr_EN[$index];
        }

    }

    public static function getAlphavitCharDec($index)
    {

        if ($index != 0) {
            return self::$let_arr_EN[$index - 1];
        } else {
            return self::$let_arr_EN[$index];
        }
    }

    public static function get_encrypt_index($text, $key)
    {
        $autokey = self::getAutoKey($text, $key);
        $encryptIndex = [];
        for ($i = 0; $i < count($text); $i++) {
            $encryptIndex[$i] = self::getEncryptIndex(self::get_Alphavit_Index($text[$i]), self::get_Alphavit_Index($autokey[$i]));

        }
        return $encryptIndex;
    }
    public static function get_encrypt_index_dec($text, $key)
    {
        $autokey = self::getAutoKey($text, $key);
        $decryptText = [];

        for ($i = 0; $i < count($text); $i++) {
            $decryptText[$i] = self::getDecryptIndex(self::get_Alphavit_Index_dec($text[$i]), self::get_Alphavit_Index_dec($autokey[$i]));
            if (($i + count($key)) < count($autokey)) {
                $autokey[$i + count($key)] = $decryptText[$i];
            }
        }
        return $decryptText;
    }

    public static function get_al_index($text)
    {
        $textindex = [];
        for ($i = 0; $i < count($text); $i++) {
            $textindex[$i] = self::get_Alphavit_Index($text[$i]);
        }
        return $textindex;
    }

    public static function getEncryptIndex($charIndex, $keyIndex)
    {

        $result = 0;
        $result = ($charIndex + $keyIndex) % count(self::$let_arr_EN);

        if (($result - 1) == -1) {
            $result = 26;
        }
        $result = $result - 1;
        if ($result == 0) {
            $result = 26;
        }
        return $result;
    }

    public static function get_Alphavit_Index($symbol)
    {

        $result = 0;
        for ($i = 0; $i < count(self::$let_arr_EN); $i++) {
            if (self::$let_arr_EN[$i] == $symbol) {
                $result = $i;
            }
        }
        return $result + 1;
    }

    public static function get_Alphavit_Index_dec($symbol)
    {

        $result = 0;
        for ($i = 0; $i < count(self::$let_arr_EN); $i++) {
            if (self::$let_arr_EN[$i] == $symbol) {
                $result = $i;
            }
        }
        return $result + 1;
    }

    public static function getAutoKey($text, $key)
    {

        $reslult = [];
        for ($i = 0; $i < count($key); $i++) {
            $reslult[$i] = $key[$i];
        }
        for ($j = count($key); $j < count($text); $j++) {
            $reslult[$j] = $text[$j - count($key)];
        }
        return $reslult;
    }
}
