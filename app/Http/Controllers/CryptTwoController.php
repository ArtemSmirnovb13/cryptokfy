<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CryptTwoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('LabTwo',
            [

            ]);
    }

    public function crypt(Request $request)
    {
        $letter = "abcdefghijklmnopqrstuvwxyz";
        $itter = 0;
        $text = $request->get('mess');
        $key = $request->get('key');


        switch ($request->input('action')) {
            case 'зашифровать':

                return view('LabTwo',
                    [
                        'messages' => $text,
                        'keys' => $key,
                        'arr_key' => str_split($key),
                        'cryptArr' => $this->encrypt($key, $text),
                        'text' => 'Зашифрованное',
                        'letter' => str_split($letter),
                        'string_letter' => $letter,
                        'itter' => $itter
                    ]);

            case 'Decrypt':
                return view('LabTwo',
                    [
                        'messages' => $text,
                        'keys' => $key,
                        'cryptArr' => $this->decrypt($key, $text),
                        'text' => 'Расшифрованное',
                    ]);
                break;
        }


    }

    function decrypt($pswd, $text)
    {
        // change key to lowercase for simplicity
        $pswd = strtolower($pswd);

        // intialize variables
        $code = "";
        $ki = 0;
        $kl = strlen($pswd);
        $length = strlen($text);

        // iterate over each line in text
        for ($i = 0; $i < $length; $i++) {
            // if the letter is alpha, decrypt it
            if (ctype_alpha($text[$i])) {
                // uppercase
                if (ctype_upper($text[$i])) {
                    $x = (ord($text[$i]) - ord("A")) - (ord($pswd[$ki]) - ord("a"));

                    if ($x < 0) {
                        $x += 26;
                    }

                    $x = $x + ord("A");

                    $text[$i] = chr($x);
                } // lowercase
                else {
                    $x = (ord($text[$i]) - ord("a")) - (ord($pswd[$ki]) - ord("a"));

                    if ($x < 0) {
                        $x += 26;
                    }

                    $x = $x + ord("a");

                    $text[$i] = chr($x);
                }

                // update the index of key
                $ki++;
                if ($ki >= $kl) {
                    $ki = 0;
                }
            }
        }

        // return the decrypted text
        return $text;
    }

    function encrypt($pswd, $text)
    {
        // change key to lowercase for simplicity
        $pswd = strtolower($pswd);

        // intialize variables
        $code = "";
        $ki = 0;
        $kl = strlen($pswd);
        $length = strlen($text);

        // iterate over each line in text
        for ($i = 0; $i < $length; $i++) {
            // if the letter is alpha, encrypt it
            if (ctype_alpha($text[$i])) {
                // uppercase
                if (ctype_upper($text[$i])) {
                    $text[$i] = chr(((ord($pswd[$ki]) - ord("a") + ord($text[$i]) - ord("A")) % 26) + ord("A"));
                } // lowercase
                else {
                    $text[$i] = chr(((ord($pswd[$ki]) - ord("a") + ord($text[$i]) - ord("a")) % 26) + ord("a"));
                }

                // update the index of key
                $ki++;
                if ($ki >= $kl) {
                    $ki = 0;
                }
            }
        }

        // return the encrypted code
        return $text;
    }

    public function MyMod($val)
    {
        return $val < 0 ? $val + 26 : $val;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
