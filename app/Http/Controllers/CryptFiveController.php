<?php

namespace App\Http\Controllers;

use App\DH;
use Illuminate\Http\Request;
use App\DH as Crypt;

class CryptFiveController extends Controller
{
    public function index(Request $request)
    {

        $getmessage = $request->get('mess');
        $getkey = $request->get('key');;
        return view('LabFive',
            [
                'messages' => $getmessage,
                'keys' => $getkey,

            ]);
    }

    public function crypting(Request $request)
    {

        $getmessage = $request->get('mess');
        $message = $getmessage;
        $Sadat = new Crypt();
        $s_public = rand(1, 500);
        $s_private = rand(1, 500);
        $m_public = rand(1, 500);
        $m_private = rand(1, 500);
        $Sadat->init($s_public, $m_public, $s_private);
        $Michael = new Crypt();
        $Michael->init($s_public, $m_public, $m_private);

        $s_partial = $Sadat->generate_partial_key();
        $m_partial = $Michael->generate_partial_key();
        $s_full = $Sadat->generate_full_key($m_partial);
        $m_full = $Michael->generate_full_key($s_partial);
        $m_encrypted = $Michael->encrypt_message($message);
        $decrypt = $Sadat->decrypt_message($m_encrypted, $m_full);

        return view('LabFive',
            [
                'messages' => $getmessage,
                'sec_key_1' => $s_private,
                'sec_key_2' => $m_private,
                'pub_key' => $s_public,
                'pub_key2' => $m_public,
                'm_encrypted' => base64_encode($m_encrypted),
                's_partial' => $s_partial,
                'm_partial' => $m_partial,
                's_full' => $s_full,
                'm_full' => $m_full,
                'decrypt' => $decrypt
            ]);

    }
}
