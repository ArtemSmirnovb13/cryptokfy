<?php

namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\crypt as Crypting;
use function Psy\bin;

class CryptTreeController extends Controller
{
    public function index(Request $request)
    {
        $getmessage = $request->get('mess');
        $getkey = $request->get('key');
        $imgEn = asset('storage/en.jpg');


        return view('LabTree',
            [
                'messages' => $getmessage,
                'keys' => $getkey,
                'img' => $imgEn
            ]);
    }


    public function crypt(Request $request)
    {

        $getmessage = $request->get('mess');
        $getkey = $request->get('key');
        $imgEn = asset('storage/en.jpg');

        switch ($request->input('action')) {
            case 'зашифровать':
                return view('LabTree',
                    [
                        'messages' => Crypting::getChars($getmessage),
                        'keys' => $getkey,
                        'Auto_key' => Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'al_index' => Crypting::get_al_index(Crypting::getChars($getmessage)),
                        'keykey' => Crypting::get_al_index(Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey))),
                        'crypt_index' => Crypting::get_encrypt_index(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'crypt' => Crypting::Encryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'img' => $imgEn,
                        'cryptArr' => Crypting::Encryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                    ]);


            case 'Decrypt':
                //   dd(Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)));
                return view('LabTree',
                    [
                        'messages' => Crypting::getChars($getmessage),
                        'keys' => $getkey,
                        'Auto_key' => Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'al_index' => Crypting::get_al_index(Crypting::getChars($getmessage)),
                        'keykey' => Crypting::get_al_index(Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey))),
                        'crypt_index' => Crypting::get_index(Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey))),
                        'crypt' => Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'img' => $imgEn,
                        'cryptArr' => Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                    ]);

                break;
        }
    }

    public function ver_index(Request $request)
    {
        $getmessage = $request->get('mess');
        $getkey = $request->get('key');
        return view('Vernam',
            [
                'messages' => $getmessage,
                'keys' => $getkey

            ]);

    }

    public function ver_crypt(Request $request)
    {
        $getmessage = $request->get('mess');
        $getkey = $request->get('key');

        $get_mess_bytes = explode(' ', $this->strigToBinary($getmessage));

        $get_key_bytes = explode(' ', $this->strigToBinary($getkey));

        $encr = [];
        $key2 = implode(self::get_gamma($get_key_bytes, $get_mess_bytes));
        $text2 = implode($get_mess_bytes);

        for ($i = 0; $i < strlen(implode($get_mess_bytes)); $i++) {
            $encr[] = +self::XORStrimmed(implode(self::get_gamma($get_key_bytes, $get_mess_bytes))[$i], implode($get_mess_bytes)[$i]);
        }

        switch ($request->input('action')) {
            case 'зашифровать':
                return view('Vernam',
                    [
                        'messages' => $getmessage,
                        'keys' => self::get_gamma($get_key_bytes, $get_mess_bytes),
                        'get_mess_bytes' => $get_mess_bytes,
                        'text' => $text2,
                        'key' => $key2,
                        'encr' => implode($encr)


                    ]);
            case 'Decrypt':

                $getmessage = $request->get('mess');
                $get_key_bytes = explode(' ', $this->strigToBinary($getkey));

                $key2 = implode(self::get_gamma($get_key_bytes, str_split($getmessage,7)));

                $dec2 = [];
                for ($i = 0; $i < strlen($key2); $i++) {
                    $dec2[] = +self::XORStrimmed($getmessage[$i], $key2[$i]);
                }

                $tt ='';
                for ($j = 0; $j < count(str_split(implode($dec2), 7)); $j++) {
                    $tt .= self::binaryToString(str_split(implode($dec2), 7)[$j]);
                }

                return view('Vernam',
                    [
                        'messages' => $getmessage,
                        'keys' => self::get_gamma($get_key_bytes, $get_mess_bytes),
                        'get_mess_bytes' => $get_mess_bytes,
                        'text' => $getmessage,
                        'key' => $key2,
                        'encr' => $tt


                    ]);
                break;
        }

    }

    public static function strigToBinary($string)
    {
        $characters = str_split($string);

        $binary = [];
        foreach ($characters as $character) {

            $data = unpack('H*', $character);

            $binary[] = base_convert($data[1], 16, 2);
        }
        for ($i = 0; $i < count($binary); $i++) {
            if ($binary[$i] == 100000) {
                $binary[$i] = '0100000';
            }
        }

        return implode(' ', $binary);
    }

    public static function binaryToString($binary)
    {
        $binaries = explode(' ', $binary);

        $string = null;
        foreach ($binaries as $binary) {
            $string .= pack('H*', dechex(bindec($binary)));
        }

        return $string;
    }

    public static function XORStrimmed($key, $text)
    {
        $result = (int)$text ^ (int)$key;
        return $result;
    }

    public static function get_gamma($key, $text)
    {
        $gammaIndex = 0;
        $result = [];
        $gamma = [];

        foreach ($text as $bb) {
            $result[] = +$bb ^ $key[$gammaIndex];
            $gamma[] = +($key[$gammaIndex]);
            if ($gammaIndex < count($key) - 1) {
                $gammaIndex++;

            } else {
                $gammaIndex = 0;

            }

        }

        return $gamma;
    }
}
