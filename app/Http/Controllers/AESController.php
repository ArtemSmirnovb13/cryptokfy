<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AESController extends Controller
{
    public function index(Request $request)
    {
        return view('AES',
        [

        ]);

    }

    public function AES(Request $request)
    {
        $message = $request->get('mess');
        $key = $request->get('key');
        switch ($request->input('action')) {
            case 'зашифровать':

                if (strlen($message) % 2 != 0) {
                    $message .= '#';
                }


                for ($i = 0; $i < count(str_split($message)); $i++) {
                    $arr[$i] = ord(str_split($message)[$i]);
                }

                $arrLR = array_chunk($arr, count($arr) / 2);

                $arrL = $arrLR[0];

                $arrR = $arrLR[1];
                $n = strlen($key);

                $crypt = $this->Crypt_round($arrL, $arrR, $n, $key);
                //dd($crypt);
                $decrypt = $this->DECrypt_round($crypt['L'], $crypt['R'], $n, $key);
                $decord = '';
                for ($i = 0; $i < count(array_merge($decrypt['L'], $decrypt['R'])); $i++) {
                    $decord .= chr((array_merge($decrypt['L'], $decrypt['R']))[$i]);
                }

                return view('AES',
                    [
                        'messages' => $message,
                        'keys' => $key,
                        'arrL' => $arrL,
                        'arrR' => $arrR,
                        'n' => $n,
                        'crypt' => array_merge($crypt['L'],$crypt['R']),
                        'decord' => $decord
                    ]);
                break;
            case 'Decrypt':

                break;
        }
    }

    public function F($L, $R, $n, $key)
    {

        $result = $R ^ (($L + $n) % ord($key));

        return $result;
    }

    public function Crypt_round($L, $R, $n, $key)
    {

        for ($i = 0; $i < $n; $i++) {

            if ($i == $n - 1) {

                for ($j = 0; $j < count($L); $j++) {

                    $Lnew = $this->F($L[$j], $R[$j], $i + 1, (str_split($key))[$i]);
                    $R[$j] = $Lnew;
                }

            } else {
                for ($j = 0; $j < count($L); $j++) {
                    $Lnew = $this->F($L[$j], $R[$j], $i + 1, (str_split($key))[$i]);

                    $R[$j] = $L[$j];
                    $L[$j] = $Lnew;
                }
            }
        }

        $arr = ['L' => $L, 'R' => $R];
        return $arr;
    }

    public function DECrypt_round($L, $R, $n, $key)
    {
        for ($i = $n - 1; $i >= 0; $i--) {

            if ($i == 0) {
                for ($j = 0; $j < count($L); $j++) {

                    $Lnew = $this->F($L[$j], $R[$j], $i + 1, (str_split($key))[$i]);
                    $R[$j] = $Lnew;
                }
            } else {
                for ($j = 0; $j < count($L); $j++) {
                    $Lnew = $this->F($L[$j], $R[$j], $i + 1, (str_split($key))[$i]);

                    $R[$j] = $L[$j];
                    $L[$j] = $Lnew;
                }
            }

        }
        $arr = ['L' => $L, 'R' => $R];
        return $arr;
    }
}
