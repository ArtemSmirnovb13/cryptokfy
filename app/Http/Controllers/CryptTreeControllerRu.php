<?php

namespace App\Http\Controllers;

use App\cryptRu as Crypting;
use Illuminate\Http\Request;

class CryptTreeControllerRu extends Controller
{
    public function index(Request $request)
    {
        $getmessage = $request->get('mess');
        $getkey = $request->get('key');
        $imgRu = asset('storage/table.png');
        return view('LabTreeRu',
            [
                'messages' => $getmessage,
                'keys' => $getkey,
                'img' => $imgRu
            ]);
    }

    public function crypt(Request $request)
    {

        $getmessage = $request->get('mess');
        $getkey = $request->get('key');
        $imgRu = asset('storage/table.png');
//        $get_mess_bytes = Crypting::GetBytes($getmessage);
//        $get_key_bytes = Crypting::GetBytes($getkey);
//
//
//        $cipherText = array_pad([], strlen($getmessage), "00");
//
//       dd($cipherText);
        //  $arr_RU = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я'];

        switch ($request->input('action')) {
            case 'зашифровать':
                return view('LabTreeRu',
                    [
                        'messages' => Crypting::getChars($getmessage),
                        'keys' => $getkey,
                        'Auto_key' => Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'al_index' => Crypting::get_al_index(Crypting::getChars($getmessage)),
                        'keykey' => Crypting::get_al_index(Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey))),
                        'crypt_index' => Crypting::get_index(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'crypt' => Crypting::Encryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'img' => $imgRu,
                        'cryptArr' => Crypting::Encryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                    ]);


            case 'Decrypt':

               // Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey));
                return view('LabTreeRu',
                    [
                        'messages' => Crypting::getChars($getmessage),
                        'keys' => $getkey,
                        'Auto_key' => Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'al_index' => Crypting::get_al_index(Crypting::getChars($getmessage)),
                        'keykey' => Crypting::get_al_index(Crypting::getAutoKey(Crypting::getChars($getmessage), Crypting::getChars($getkey))),
                        'crypt_index' => Crypting::get_index(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'crypt' => Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                        'img' => $imgRu,
                        'cryptArr' => Crypting::Decryption(Crypting::getChars($getmessage), Crypting::getChars($getkey)),
                    ]);

                break;
        }
    }
}
