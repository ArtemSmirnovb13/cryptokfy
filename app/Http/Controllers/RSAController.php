<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\RSA;
class RSAController extends Controller
{
    public function index(Request $request) {

        return view('RSA',
            [


            ]);
    }

    public function RSA(Request $request) {
        $RSA = new RSA();

        $p = $RSA->SimpleGenerate();
        $q = $RSA->SimpleGenerate();
        while ($p == $q){
            $p = $RSA->SimpleGenerate();
            $q = $RSA->SimpleGenerate();
        }
       if ($request->get('mess') != null) {


           if (gmp_prob_prime($p) == 2 && gmp_prob_prime($q) == 2) {
               //dd(gmp_intval(gmp_gcd($p,$q)));

               $n = $RSA->initPQParams($p, $q)['n'];
               //dd($n);

               $ph = $RSA->initPQParams($p, $q)['ph'];
               // dd($ph);
               $e = $RSA->initE($ph);

               $d = $RSA->initEncryptingKey($e, $ph);


               $openkey = ['e' => $e, 'n' => $n]; //Данны ключ есть у отправителя
               $secretkey = ['d' => $d, 'n' => $n];

               $message = $request->get('mess');
               $encrypt_message = $RSA->RSA_ENCRYPT($message, $openkey);

               $decrypt_message = $RSA->RSA_DECRYPT($encrypt_message, $secretkey);

               return view('RSA',
                   [
                       'messages' => $message,
                       'p' => $p,
                       'q' => $q,
                       'n' => $n,
                       'ph' => $ph,
                       'e' => $e,
                       'd' => $d,
                       'encrypt_message' => $encrypt_message,
                       'decrypt_message' => $decrypt_message

                   ]);
           } else {
               return view('RSA')->withErrors(['Числа не простые']);
           }
       }else {
           return view('RSA')->withErrors(['Вы не ввели сообщение']);
       }
    }
}
