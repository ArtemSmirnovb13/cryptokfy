<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CryptOneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $getmessage = $request->get('mess');
        $getkey = $request->get('key');

        return view('LabOne',
            [
                'messages' => $getmessage,
                'keys' => $getkey
            ]);
    }

    public function crypt(Request $request)
    {

        $request->validate([
            'mess' => 'required',
            'key' => 'required'
        ]);
        $getmessage = $request->get('mess');
        $getkey = strtolower($request->get('key'));

        switch ($request->input('action')) {
            case 'зашифровать':


                if (fmod(strlen($getmessage), strlen($getkey)) != null) {
                    $n = ceil(fmod(strlen($getmessage), strlen($getkey)));
                    $m = strlen($getkey) - $n;

                    for ($i = 0; $i < $m; $i++) {
                        $getmessage = $getmessage . ' ';
                    }
                }

                $count = ceil(strlen($getmessage) / strlen($getkey));
                $string_3 = str_split($getmessage);

                $finall_string_3 = array_chunk($string_3, strlen($getkey));

                $out = [];
                for ($i = 0; $i < $count; $i++) {
                    foreach ($this->key_for($getkey) as $value) {
                        $out[] = $finall_string_3[$i][$value];
                    }
                }


                    $test = array_chunk($out, strlen($getkey));
                    $decode = array_chunk($this->temp_decrypt(implode('',$out),$getkey),strlen($getkey));


                return view('LabOne',
                    [
                        'messages' => $getmessage,
                        'keys' => $getkey,
                        'cryptArr' => $out,
                        'text' => 'Зашифрованное',
                        'test' => $test,
                        'decode' => $decode
                    ]);
                break;

            case 'Decrypt':
                $cryptArr = $this->decrypt($request);
                $dec = array_chunk($cryptArr, strlen($getkey));
                return view('LabOne',
                    [
                        'messages' => $getmessage,
                        'keys' => $getkey,
                        'cryptArr' => $cryptArr,
                        'text' => 'Расшифрованное =)',
                        'decode' => $dec
                    ]);
                break;

        }

    }

    public function key_for($key)
    {
        for ($i = 0; $i < strlen($key); ++$i) {
            $arr[$i] = $i;
        }
        array_multisort(str_split($key), $arr);

        return $arr;
    }

    public function decrypt(Request $request)
    {
        $request->validate([
            'mess' => 'required',
            'key' => 'required'
        ]);
        $getmessage = $request->get('mess');
        $getkey = strtolower($request->get('key'));
        if (fmod(strlen($getmessage), strlen($getkey)) != null) {
            $n = ceil(fmod(strlen($getmessage), strlen($getkey)));
            $m = strlen($getkey) - $n;

            for ($i = 0; $i < $m; $i++) {
                $getmessage = $getmessage . ' ';
            }
        }

        $count = ceil(strlen($getmessage) / strlen($getkey));
        $string_3 = str_split($getmessage);
        $finall_string_3 = array_chunk($string_3, strlen($getkey));

        $out = [];

        $ar1 = $this->key_for($getkey);

        for ($i = 0; $i < $count; $i++) {
            foreach ($this->key_for(implode('', $ar1)) as $value) {
                $out[] = $finall_string_3[$i][$value];
            }
        }

        return $out;
    }

    public function temp_decrypt($string,$key)
    {

        $getmessage = $string;
        $getkey = strtolower($key);
        if (fmod(strlen($getmessage), strlen($getkey)) != null) {
            $n = ceil(fmod(strlen($getmessage), strlen($getkey)));
            $m = strlen($getkey) - $n;

            for ($i = 0; $i < $m; $i++) {
                $getmessage = $getmessage . ' ';
            }
        }

        $count = ceil(strlen($getmessage) / strlen($getkey));
        $string_3 = str_split($getmessage);
        $finall_string_3 = array_chunk($string_3, strlen($getkey));

        $out = [];

        $ar1 = $this->key_for($getkey);

        for ($i = 0; $i < $count; $i++) {
            foreach ($this->key_for(implode('', $ar1)) as $value) {
                $out[] = $finall_string_3[$i][$value];
            }
        }

        return $out;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
