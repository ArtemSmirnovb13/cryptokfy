<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cryptRu extends Model
{
    public static $arr_RU = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ','Ы','Ь', 'Э', 'Ю', 'Я'];

    public static function GetBytes($get)
    {
        $getbytes = [];
        for ($i = 0; $i < strlen($get); $i++) {
            $getbytes[$i] = dechex(ord($get[$i]));
        }
        return $getbytes;
    }

    public static function getChars($text)
    {

        $str = str_replace(' ', '', $text);

        $str = mb_strtoupper($str);

        $str = preg_split('//u', $str, null, PREG_SPLIT_NO_EMPTY);

        return $str;
    }

    public static function Encryption($text, $key)
    {

        $autokey = self::getAutoKey($text, $key);

        $encryptText = [];

        for ($i = 0; $i < count($text); $i++) {

            $encryptText[$i] = self::getAlphavitChar(self::getEncryptIndex(self::get_Alphavit_Index($text[$i]), self::get_Alphavit_Index($autokey[$i])));
        }
        //  dd(self::getEncryptIndex(self::get_Alphavit_Index($text[$i]), self::get_Alphavit_Index($autokey[$i])));

        return $encryptText;
    }

    public static function Decryption($text,$key){
        $autokey = [];
        $decryptText =[];
        for ($i = 0; $i < count($key); $i++) {
            $autokey[$i] = $key[$i];
        }
        for ($i = 0; $i < count($text) - count($key); $i++) {
            $autokey[$i + count($key)] = 0;
        }

        for ($i = 0; $i < count($text); $i++) {

            $decryptText[$i] = self::getAlphavitCharDec(self::getDecryptIndex(self::get_Alphavit_Index_dec($text[$i]),self::get_Alphavit_Index_dec($autokey[$i])));
            if (($i + count($key)) < count($autokey)) {
                $autokey[$i + count($key)] = $decryptText[$i];
            }
        }
        return $decryptText;
    }

    public static function get_index($text, $key)
    {
        $autokey = [];
        $decryptText =[];
        for ($i = 0; $i < count($key); $i++) {
            $autokey[$i] = $key[$i];
        }
        for ($i = 0; $i < count($text) - count($key); $i++) {
            $autokey[$i + count($key)] = 0;
        }

        for ($i = 0; $i < count($text); $i++) {

            $decryptText[$i] = self::getDecryptIndex(self::get_Alphavit_Index_dec($text[$i]),self::get_Alphavit_Index_dec($autokey[$i])) + 1;
            if (($i + count($key)) < count($autokey)) {
                $autokey[$i + count($key)] = $decryptText[$i];
            }
        }

        return $decryptText;
    }

    public static function getAlphavitChar($index) {

            return self::$arr_RU[$index];

    }
    public static function getAlphavitCharDec($index) {

        return self::$arr_RU[$index];

    }

    public static function get_encrypt_index($text, $key)
    {
        $autokey = self::getAutoKey($text, $key);
        $encryptIndex = [];
        for ($i = 0; $i < count($text); $i++) {
            $encryptIndex[$i] = self::getEncryptIndex(self::get_Alphavit_Index($text[$i]), self::get_Alphavit_Index($autokey[$i]) + 1);

        }

        return $encryptIndex;
    }

    public static function get_al_index($text)
    {
        $textindex = [];
        for ($i = 0; $i < count($text); $i++) {
            $textindex[$i] = self::get_Alphavit_Index($text[$i]);
        }
        return $textindex;
    }

    public static function getEncryptIndex($charIndex, $keyIndex)
    {

        $result = 0;
        $result = ($charIndex + $keyIndex) % count(self::$arr_RU);

        if (($result - 1) == -1) {
            $result = 33;
        }

        return $result - 1;
    }

    public static function getDecryptIndex($encryptIndex, $keyIndex){
        $relust = 0;

        if ($encryptIndex >= $keyIndex){
            $relust = $encryptIndex - $keyIndex;
        }
        else {
            $relust = ( $encryptIndex + count(self::$arr_RU)) - $keyIndex;
        }

        if (($relust - 1) == -1) {
            $relust = 33;
        }
        return $relust - 1;
    }

    public static function get_Alphavit_Index_dec($symbol)
    {

        $result = 0;
        for ($i = 0; $i < count(self::$arr_RU); $i++) {
            if (self::$arr_RU[$i] == $symbol) {
                $result = $i;
            }
        }
        return $result + 1;
    }

    public static function get_Alphavit_Index($symbol)
    {

        $result = 0;
        for ($i = 0; $i < count(self::$arr_RU); $i++) {
            if (self::$arr_RU[$i] == $symbol) {
                $result = $i;
            }
        }
        return $result + 1;
    }

    public static function getAutoKey($text, $key)
    {

        $reslult = [];
        for ($i = 0; $i < count($key); $i++) {
            $reslult[$i] = $key[$i];
        }
        for ($j = count($key); $j < count($text); $j++) {
            $reslult[$j] = $text[$j - count($key)];
        }
        return $reslult;
    }
}
