<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RSA extends Model
{
    public $decriptedData;
    public $encriptedData;

    public function initPQParams($p, $q)
    {
        if ($p && $q && ($q != $p)) {
            $n = $p * $q;
            $ph = ($p - 1) * ($q - 1);
        }
        $arr = ['n' => $n, 'ph' => $ph];
        return $arr;
    }

    public function initEncryptingKey($e, $ph, $d = 0)
    {
        while (gmp_intval(gmp_mod(($d * $e), $ph)) != 1) {
            $d += 1;
            if ($d == $e) {
                $d += 1;
            }
        }
        return $d;
    }

    public function initE($ph, $e = 2)
    {

        while (gmp_gcd($ph, $e) != 1) {
            $e += 1;

        }
        return $e;
    }

    public function RSA_ENCRYPT($mess, $openkey)
    {

        for ($j = 0; $j < strlen($mess); $j++) {
            $b = ord($mess[$j]);

            $result = 1;
         //   for ($i = 0; $i < $openkey['e']; $i++) {
                $result = gmp_intval(gmp_mod((gmp_pow($b, $openkey['e'])), $openkey['n']));
          //  }

            $this->encriptedData[$j] = $result;
        }

        //$encrypt = gmp_intval(gmp_mod((gmp_pow($mess, $openkey['e'])), $openkey['n']));

        return $this->encriptedData;
    }

    public function SimpleGenerate()
    {
        $number = rand(50,99);
        while (gmp_prob_prime($number) != 2) {
            $number = rand(50,99);
        }
        return $number;
    }

    public function RSA_DECRYPT($encrypt_message, $secretkey)
    {

        for ($j = 0; $j < count($encrypt_message); $j++) {
            $b = ($encrypt_message[$j]);
            $result = 1;
          //  for ($i = 0; $i < $secretkey['d']; $i++) {
                $result = gmp_intval(gmp_mod((gmp_pow($b, $secretkey['d'])), $secretkey['n']));
          //  }
            $this->decriptedData .= chr($result);
        }

        // dd($this->encriptedData);
        // $decrypt = gmp_intval(gmp_mod((gmp_pow($encrypt_message, $secretkey['d'])), $secretkey['n']));
        return $this->decriptedData;
    }
}
