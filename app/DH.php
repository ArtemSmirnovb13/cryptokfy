<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function Psy\bin;

class DH extends Model
{

    public $public_key1;
    public $public_key2;
    public $private_key;
    public $full_key;

    function init($public_key1, $public_key2, $private_key)
    {

        $this->public_key1 = $public_key1;
        $this->public_key2 = $public_key2;
        $this->private_key = $private_key;

    }

    public function generate_partial_key()
    {
        $partial_key = bcpow($this->public_key1, $this->private_key);
        $partial_key = bcmod($partial_key, $this->public_key2);
        return (int)$partial_key;
    }

    public function generate_full_key($partial_key_r)
    {
        $full_key = bcpow($partial_key_r, $this->private_key);
        $full_key = bcmod($full_key, $this->public_key2);
        $this->full_key = $full_key;
        return (int)$full_key;
    }

    public function encrypt_message($message)
    {
        $encrypted_message = '';
        $key = $this->full_key;

        foreach (str_split($message) as $mess) {
            $encrypted_message .= chr(ord($mess) + $key);
        }

        return (string)$encrypted_message;
    }

    public function decrypt_message($encrypted_message, $key)
    {
        $decrypted_message = '';
        foreach (str_split($encrypted_message) as $mess) {
            $decrypted_message .= chr(ord($mess) - $key);
        }
        return $decrypted_message;
    }

}
