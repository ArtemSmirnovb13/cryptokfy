@extends('layouts.app')

@section('content')
    @include('include.errors')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <form action="{{route('RSA.crypting')}}" method="post">
                                {{csrf_field()}}
                                {{ method_field('POST') }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Сообщение</label>
                                    <input type="text" class="form-control" name="mess" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="@if(isset($m_encrypted))@endif">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                        anyone else.</small>
                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>

                    </div>
                    @if(isset($messages))
                        <div class="container">
                            <div class="alert alert-success" role="alert">
                                Пользователь ввёл сообщение : {{$messages}}
                            </div>
                            <div class="alert alert-success" role="alert">
                                p = {{$p}} : q = {{$q}} <br>
                                n = {{$n}}  <br>
                                Функция Эйлера = {{$ph}} <br>
                                e = {{$e}}   <br>
                                d = {{$d}} : (d×е)%φ=1 <br>
                                Пара чисел {e, n} — это  открытый ключ { {{$e}}, {{$n}} } <br>
                                Пара {d, n} — это секретный ключ { {{$d}}, {{$n}} } <br>
                                Зашифр. Сообщение при помощи Пары чисел собеседника {e, n}: {{implode($encrypt_message)}}<br>
                                Расшифр. Сообщение при помощи секретки {d, n} : {{$decrypt_message}}
                            </div>

                        </div>


                    @endif
                </div>

            </div>

        </div>

    </div>

@endsection
