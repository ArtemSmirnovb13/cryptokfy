@extends('layouts.app')

@section('content')
    @include('include.errors')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form class="form-horizontal" action="{{route('CryptTree.crypt.go')}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('POST') }}
                            {{--form include--}}
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <label for="">Выберите язык</label>

                                    </div>
                                    <div class="col-sm">
                                        <div class="col-sm pb-2" style="display: grid">
                                            <a href="{{route('CryptTree.crypt.ru')}}"
                                               class="btn btn-primary mybutton">Русский</a>
                                        </div>
                                    <div class="col-sm pb-2" style="display: grid">
                                        <a href="{{route('CryptTree.crypt')}}"
                                           class="btn btn-primary mybutton">Ангийский</a>
                                    </div></div>
                                <div class="col-sm pb-2" style="display: grid">
                                    <a href="{{route('CryptTree.Vernam')}}"
                                       class="btn btn-primary mybutton">Vernam</a>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm">
                                        <label for="">Сообщение</label>
                                        <input class="form-control" type="text" placeholder="text"
                                               name="mess">
                                    </div>
                                    <div class="col-sm">
                                        <label for="">Ключ</label>
                                        <input class="form-control" type="text" name="key"
                                               placeholder="key">
                                    </div>
                                </div>
                                <hr/>

                                <hr/>

                                <div class="row myrow">

                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="зашифровать">

                                    </div>

                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="Decrypt">
                                    </div>
                                    <div class="col-sm pb-2" style="display: grid">
                                        <a href="{{route('CryptTree.crypt')}}"
                                           class="btn btn-primary mybutton">Отчистить</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                <div class="container">
                    @if(isset($cryptArr))

                        <div class="alert alert-danger" role="alert">
                            сообщение: {{implode('',$cryptArr) }}
                        </div>


                    @endif
                </div>

                </div>


            </div>
            @if(isset($messages))
            <table class="table table-bordered table-hover">

                <thead>
                <tr>
                    @foreach($messages as  $te)
                        <th scope="col">{{$te}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($al_index as  $index)
                        <th scope="col">{{$index}}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($Auto_key as  $Auto)
                        <th scope="col">{{$Auto}}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($keykey as  $k)
                        <th scope="col">{{$k}}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($crypt as  $index)
                        <th scope="col">{{$index}}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($crypt_index as  $index)
                        <th scope="col">{{$index}}</th>
                    @endforeach
                </tr>
                <tr>

                </tr>
                </tbody>
            </table>
                @endif
           <img src="{{$img}}" class="img-fluid" alt="Responsive image">
        </div>
    </div>
    <br>

@endsection
