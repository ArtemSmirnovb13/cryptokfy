@extends('layouts.app')

@section('content')
    @include('include.errors')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form class="form-horizontal" action="{{route('CryptTwo.crypt')}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('POST') }}
                            {{--form include--}}
                            <div class="container">

                                <div class="row">
                                    <div class="col-sm">
                                        <label for="">Сообщение</label>
                                        <input class="form-control" type="text" placeholder="text"
                                               name="mess">
                                    </div>
                                    <div class="col-sm">
                                        <label for="">Ключ</label>
                                        <input class="form-control" type="text" name="key"
                                               placeholder="key">
                                    </div>
                                </div>
                                <hr/>

                                <hr/>

                                <div class="row myrow">

                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="зашифровать">

                                    </div>

                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="Decrypt">
                                    </div>
                                    <div class="col-sm pb-2" style="display: grid">
                                        <a href="{{route('CryptTwo.index')}}"
                                           class="btn btn-primary mybutton">Отчистить</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="container">
                        @if(isset($messages) and isset($keys))
                            <div class="alert alert-primary" role="alert">
                                Сообщение: {{$messages}} <br>
                                Ключ: {{$keys}}
                            </div>

                            <div class="alert alert-danger" role="alert">
                                {{$text}} сообщение: {{$cryptArr }}
                            </div>


                        @endif
                    </div>

                </div>


            </div>

        </div>
    </div>
    <br>
    <div class="container">
        @if(isset($letter))
            <table class="table table-bordered table-hover">

                <thead>
                <tr>
                    @foreach(str_split(str_replace(' ', '' ,$messages)) as  $te)
                        <th scope="col">{{$te}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>

                <tr>
                    @for($i = 0; $i < strlen(str_replace(' ', '' ,$messages)); $i++)
                        <td>{{$arr_key[$itter]}}</td>
                        <div class="btn" style="visibility: hidden">{{$itter += 1}}</div>
                        @if($itter >= strlen($keys))
                            <div class="btn" style="visibility: hidden">{{$itter = 0}}</div>
                        @endif


                    @endfor
                </tr>

                <tr>
                    @foreach(str_split(str_replace(' ', '' ,$cryptArr)) as  $te)
                        <th scope="col">{{$te}}</th>
                    @endforeach
                </tr>
                </tbody>
            </table>
        @endif

        <br>
        @if(isset($letter))
            <table class="table table-bordered table-hover">

                <thead>
                <tr>
                    @foreach($letter as  $te)
                        <th scope="col">{{$te}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>

                @for($i = 0; $i < strtolower(strlen($keys)); $i++)
                    <tr>
                        @for($j = array_search(strtolower($keys[$i]), $letter); $j < strlen($string_letter); $j++)
                            <td>{{$letter[$j]}}</td>
                        @endfor
                        @for($q = 0; $q < array_search(strtolower($keys[$i]), $letter); $q++)
                            <td>{{$letter[$q]}}</td>
                        @endfor

                    </tr>
                @endfor

                </tbody>
            </table>
        @endif





    </div>
@endsection
