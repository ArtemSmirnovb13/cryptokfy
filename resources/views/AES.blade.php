@extends('layouts.app')

@section('content')
    @include('include.errors')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form class="form-horizontal" action="{{route('AES.crypting')}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('POST') }}
                            {{--form include--}}
                            <div class="container">

                                <div class="row">
                                    <div class="col-sm">
                                        <label for="">Сообщение</label>
                                        <input class="form-control" type="text" placeholder="text"
                                               name="mess">
                                    </div>
                                    <div class="col-sm">
                                        <label for="">Ключ</label>
                                        <input class="form-control" type="text" name="key"
                                               placeholder="exaple@mail.ru">
                                    </div>
                                </div>
                                <hr/>

                                <hr/>

                                <div class="row myrow">

                                    <div class="col-sm pb-2" style="display: grid">
                                        <input class="btn btn-primary" name="action" type="submit" value="зашифровать">

                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="container">
                        @if(isset($messages) and isset($keys))
                            <div class="alert alert-primary" role="alert">
                                Сообщение: {{$messages}} <br>
                                Ключ: {{$keys}}
                            </div>

                            <div class="alert alert-danger" role="alert">
                                 -L EFT  : {{implode($arrL)}}<br>
                                RIGHT : {{implode($arrR)}}<br>
                                After {{$n}} Rounds encr message is :<br>
                                {{implode($crypt)}} <br>

                            </div>
                            <div class="alert alert-success" role="alert">
                                Расшифрованное сообщение по ключу : {{implode($crypt)}} <br>

                                Будет : {{$decord}}
                            </div>

                        @endif
                    </div>

                </div>


                <div class="container">
                    @if(isset($test))
                    <div class="alert alert-success" role="alert">
                        Демонстрационный массив
                    </div>

                    <table class="table table-bordered table-hover">

                        <tbody>

                        @forelse($test as  $te => $t)
                            <tr>
                                @foreach($t as $tt)
                                    <td>{{$tt}}</td>
                                @endforeach
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center"><h2>Данные отстутсвуют</h2></td>
                            </tr>
                        @endforelse
                        </tbody>

                    </table>
                    @endif
                        @if(isset($decode))
                            <div class="alert alert-info" role="alert">
                                A simple info alert—check it out!
                            </div>

                            <table class="table table-bordered table-hover">

                                <tbody>

                                @forelse($decode as $dec => $t)
                                    <tr>
                                        @foreach($t as $tt)
                                            <td>{{$tt}}</td>
                                        @endforeach
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" class="text-center"><h2>Данные отстутсвуют</h2></td>
                                    </tr>
                                @endforelse
                                </tbody>

                            </table>
                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection
