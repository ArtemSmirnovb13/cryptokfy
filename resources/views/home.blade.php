@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <table class="table">

                                        <tbody>
                                        <tr>
                                            <th scope="row"><a href="{{route('CryptOne.index')}}" class="btn btn-primary mybutton">Лаб 1</a></th>
                                            <th scope="row"><a href="{{route('CryptTwo.index')}}" class="btn btn-primary mybutton">Лаб 2</a></th>
                                            <th scope="row"><a href="{{route('CryptTree.crypt')}}" class="btn btn-primary mybutton">Лаб 3</a></th>
                                        </tr>
                                        <tr>
                                            <th scope="row"><a href="{{route('aes')}}" class="btn btn-primary mybutton">Лаб 4</a></th>
                                            <th scope="row"><a href="{{route('CryptFive')}}" class="btn btn-primary mybutton">Лаб 5</a></th>
                                            <th scope="row"><a href="{{route('rsa')}}" class="btn btn-primary mybutton">RSA</a></th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
