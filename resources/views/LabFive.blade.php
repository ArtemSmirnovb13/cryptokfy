@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="container">
                            <form action="{{route('CryptFive.crypting')}}" method="post">
                                {{csrf_field()}}
                                {{ method_field('POST') }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Сообщение</label>
                                    <input type="text" class="form-control" name="mess" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="@if(isset($m_encrypted))@endif">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                        anyone else.</small>
                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>

                    </div>
                    @if(isset($m_encrypted))
                        <div class="container">
                            <div class="alert alert-success" role="alert">
                                Пользователь 1 ввёл сообщение : {{$messages}}
                            </div>
                            <div class="alert alert-success" role="alert">
                                Секретный ключ пользователя 1 : {{$sec_key_1}} <br>
                                Публичный ключ пользователя 1 : {{$pub_key}} <br>
                                Промежуточный ключ: {{$s_partial}} <br>
                                Финальный ключ: {{$s_full}} <br>
                                Зашифрованное сообщение : {{($m_encrypted)}}
                            </div>

                        </div>

                        <div class="container">
                            <div class="alert alert-danger" role="alert">
                                Пользователь 2 получил сообщение : {{$m_encrypted}}
                            </div>
                            <div class="alert alert-danger" role="alert">
                                Секретный ключ пользователя 2 : {{$sec_key_2}} <br>
                                Публичный ключ пользователя 2 : {{$pub_key2}} <br>
                                Промежуточный ключ: {{$m_partial}} <br>
                                Финальный ключ: {{$m_full}} <br>
                                Дешиф сообщение : {{$decrypt}}
                            </div>

                        </div>
                    @endif
                </div>

            </div>

        </div>

    </div>

@endsection
