@extends('layouts.app')

@section('content')
    @include('include.errors')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form class="form-horizontal" action="{{route('CryptTree.Vernam.ver_crypt')}}" method="post">
                            {{csrf_field()}}
                            {{ method_field('POST') }}
                            {{--form include--}}
                            <div class="container">

                            <div class="row">
                                <div class="col-sm">
                                    <label for="">Сообщение</label>
                                    <input class="form-control" type="text" placeholder="text"
                                           name="mess">
                                </div>
                                <div class="col-sm">
                                    <label for="">Ключ</label>
                                    <input class="form-control" type="text" name="key"
                                           placeholder="key">
                                </div>
                            </div>
                            <hr/>

                            <hr/>

                            <div class="row myrow">

                                <div class="col-sm pb-2" style="display: grid">
                                    <input class="btn btn-primary" name="action" type="submit" value="зашифровать">

                                </div>

                                <div class="col-sm pb-2" style="display: grid">
                                    <input class="btn btn-primary" name="action" type="submit" value="Decrypt">
                                </div>
                                <div class="col-sm pb-2" style="display: grid">
                                    <a href=""
                                       class="btn btn-primary mybutton">Отчистить</a>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>

                @if(isset($messages))
                    <table class="table table-bordered table-hover">

                        <thead>
                        <tr>
                            @foreach(str_split($messages) as $te)
                                <th scope="col">{{$te}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($get_mess_bytes as $te)
                                <th scope="col">{{($te)}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($keys as $te)
                                <th scope="col">{{App\Http\Controllers\CryptTreeController::binaryToString($te)}}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($keys as $te)
                                <th scope="col">{{(($te))}}</th>
                            @endforeach
                        </tr>

                        </tbody>
                    </table>
                @endif
            </div>

        </div>
        @if(isset($text))
            <div class="form-group">
                <label for="exampleFormControlTextarea1">mess</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">{{$text}}</textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">key</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">{{$key }}</textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">result</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">{{$encr }}</textarea>
            </div>
        @endif
    </div>
    <br>

@endsection
